# Easywrap

#### What is Easywrap?

* Is not a framework !!! Is e simple scaffolding to create templates html!!!
* The CSS grid is written with Sass and the HTML with Pug
* Based in [BEM methodology](https://en.bem.info/method/)
* Like Bootstrap haves the standard breakpoints (xs, sm, md, lg)
* Unlike Bootstrap, the columns are separated with the left margin from the second element, example: ` [width: 70px)] + [margin-left: 30px] + [width: 70px]...` up to 1170px with [12 Columns](http://960.gs/)
* Don't have elements like buttons group , input group, group of alert, etc ... because it is a base grid to start a project with some particular graphic where Bootstrap components are not necessary
* You need to create your components!!!

### Version
#### 1.0.2

### New Features

* Migrate Jade to Pug (html preprocessor)

### Requirements

* [Nodejs](https://nodejs.org) current: 4.4.5
* [Gulpjs](http://gulpjs.com)  current: 3.9.1 | clic: 3.9.1

### Getting Started

* [Install Nodejs](https://nodejs.org/en/download/)
* [Install Gulpjs](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

You need GulpJs globally:

```bash

npm install -g gulp

```

Get the project code source



```bash

git clone https://github.com/mariolavoro85/easywrap.git
cd easywrap
npm install
gulp

```

#### Project structure

    /root
        |- app/
    		|- css (css compiled)
    		|- fonts (implement Font OpenSans)
    		|- img
    		|- js
    		|- *.html (html compiled from *.pug)
    	|- src/ (dev environment)
    		|- icons  (Icon for sprites)
    		|- images (Optimize images size in bytes)
    		|- pug	  (Templates Engine)
    		|- sass	  (Sass source)
#### Default environment configuration (config.scss)

```scss

//
// Variables
// --------------------------------------------------

//== Grid system

// Should it be responsive?
$responsive: true;

//** Number of columns in the grid.
$grid-columns:              12;

//** Margin between columns.
// Gets divided in half for the left and right.
$grid-gutter-width:         30px;

// Grid Sizes 
// (1140gs default) { 1200 / 12 - gutter } { (70 + 30) * 12 - (30 / 2) }
$column-width: 				70px;


// Grid (960gs) { 990 / 12 - gutter } { (52.5 + 30) * 12 - (30 / 2) }
//$column-width: 			52.5px;


//## Define the breakpoints at which your layout will change, adapting to different screen sizes.
$screen-xs:                  480px;
$screen-sm:                  768px;
$screen-md:                  1024px;
$screen-lg:                  1200px;

// So media queries don't overlap when required, provide a maximum
$screen-xs-max:              ($screen-sm - 1);
$screen-sm-max:              ($screen-md - 1);
$screen-md-max:              ($screen-lg - 1);


//== Typography

$default-font-name-regular:  opensans_regular;
$default-font-name-italic:   opensans_italic;
$default-font-name-semibold: opensans_semibold;
$default-font-name-bold:     opensans_bold;

//## Font, line-height, and color for body text, headings, and more.

$font-family-sans-serif:  Helvetica Neue, Helvetica, Arial, sans-serif;
$font-family-serif:       Georgia, Times New Roman, Times, serif;

//** Default monospace fonts for `<code>`, `<kbd>`, and `<pre>`.
$font-family-monospace:   Menlo, Monaco, Consolas, Courier New, monospace;

//** Default Scaffolding <body>
$font-family-base:        $default-font-name-regular,$font-family-sans-serif;

$font-size-base:          14px;
$font-size-xs:            ceil(($font-size-base * 0.85)); // ~12px
$font-size-sm:            ceil(($font-size-base * 1.25)); // ~18px
$font-size-md:            ceil(($font-size-base * 1.7));  // ~24px
$font-size-lg:            floor(($font-size-base * 2.15));// ~30px
$font-size-xlg:           floor(($font-size-base * 2.6)); // ~36px

//** `line-height` for use in components like buttons.
$line-height-base:        1.428571429; // 20/14

//** Computed "line-height" (`font-size` * `line-height`) for use with `margin`, `padding`, etc.
$line-height-computed:    floor(($font-size-base * $line-height-base)); // ~20px

//## Define common padding and border radius sizes and more. Values based on 14px text and 1.428 line-height (~20px to start).

$padding-base-vertical:     4px;
$padding-base-horizontal:   ($padding-base-vertical * 3);
$border-radius-base:        0px;


//== Colors declare (import from palette files)

$border-default:         $gray-lighter;


//** Background color for `<body>`.
$body-bg:               $White;
//** Global text color on `<body>`.
$text-color:            $gray-dark;

//** Global textual link color.
$link-color:            $ModerateBlue;

//** Link hover color set via `darken()` function.
$link-hover-color:      darken($link-color, 15%);

//** Link hover decoration.
$link-hover-decoration: underline;


```

#### Grid Main Class

```css
.grid{}
.grid__wrap{}
.grid__row{}
.grid__cell{}
.grid__cell--clear{}
.grid__cell--modifier{}
.grid__box{}
.grid__box--clear{}
.grid__box--modifier{}


```

#### Other inner components class

```css
.heading{}
.heading--modifier{}

.text{}
.text--modifier{}

.img{}
.img--modifier{}

.list{}
.list--modifier{}

.nav{}
.nav__item{}


```

#### Classic html markup

```html
<main class="grid">
	<section class="grid__wrap">
		<div class="grid__row">
			<article class="grid__cell">
				<div class="grid__box">
					... TODO
				</div>
			</article>
		</div>
	</section>
</main>

```

#### You can customize the grid cell system for the different breakpoint

```html
<article class="grid__cell grid__cell--sm-6 grid__cell--md-8">
  <div class="grid__box">
  	...Left content
  </div>
</article>
<article class="grid__cell grid__cell--sm-6 grid__cell--md-4">
  <div class="grid__box">
  	...Right content
  </div>
</article>


```

#### Remove margin-left from specific cell

```html
<article class="grid__cell grid__cell--sm-4">
  <div class="grid__box">
  	...
  </div>
</article>
<article class="grid__cell grid__cell--sm-8 grid__cell--clear">
  <div class="grid__box">
  	...
  </div>
</article>


```

#### Remove margin-left from specific cell on specific breakpoint

```html
<article class="grid__cell grid__cell--sm-5">
  <div class="grid__box">
  	...
  </div>
</article>
<article class="grid__cell grid__cell--sm-6 grid__cell--sm-clear">
  <div class="grid__box">
  	...
  </div>
</article>

```

#### Combine components

```html
<div class="grid__box">
	<hgroup>
		<h1 class="heading heading--lg heading--center">
			Your title here
		</h1>
		<h3 class="heading heading--sm">Lorem ipsum dolor sit amet</h3>
	</hgroup>
	<img class="img img--full" src="http://lorempixel.com/400/400/people">
	<p class="text text--xs">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	<ul class="list list--unstyled">
		<li><a href="#" target="_blank">Link here</a></li>
	</ul>
</div>

```

#### Embed blocks on another blocks and modify this

html:

```html
<div class="grid__box card">
  <h3 class="heading heading--xs card__title">
  	<span class="icon icon--search card__icon"></span>
  	Search icon on block News
  </h3>
</div>

```

css :

```css

/* Default Title Block */
.heading {
	padding:15px;
	font-size:18px;
	color:red;
}
.heading--xs {
	font-size:14px;
}
/* Default Icon Block */
.icon {
	position: relative;
}
/* specific icons */
.icon--search {
	backgroud:url(...);
}

/* Add the new Card Block and sub elements */
.news {
    background: orange;
}
.new__title {
	color: white;
}
.news__icon {
	margin-top:5px;
	margin-left:3px;
}

```

License
----
And of course: [MIT](https://bitbucket.com/lumapees/easywrap/blob/develop/LICENSE)
