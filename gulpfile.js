/**
 * @see http://www.mikestreety.co.uk/blog/an-advanced-gulpjs-file
 */
var base_environment = "./";
var node_sources = base_environment+"node_modules/";
// Define base folders
var src = 'src/';
var root = 'app/';
var dist = 'dist/';
/****************************************************\
*
*       LOAD PLUGINS
*
\****************************************************/
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

var browserSync = require(node_sources+'browser-sync');
var syncReload      = browserSync.reload;
// @see: https://www.browsersync.io/docs/options/
var browserSyncOptions = {
    // Open the localhost URL
    open: "local",
    // Open the site in Chrome
    browser: "google chrome",
    // don't auto-reload all browsers following a Browsersync reload
    reloadOnRestart: false,
    notify: false,
    port: 8001,
    server: {
        baseDir: root,
        index: "index.html"
    }
  }
/****************************************************\
*
*       DEV
*
\****************************************************/
// start local server
gulp.task('server', function buildServer(){
  browserSync(browserSyncOptions);
});

// compress images size
gulp.task('images', function buildImages() {
  return gulp.src(src + 'images/**/*')
    .pipe(plugins.cache(plugins.imagemin({
                 optimizationLevel: 5,
                 progressive: true,
                 interlaced: true
               })))
    .pipe(gulp.dest(root + 'images-optimized'));
});

// css sprites
gulp.task('sprite', function buildSprites() {

  var spritesmith = require(node_sources+'gulp.spritesmith');

  var spriteData = gulp.src(src + 'icons/*.png')
    .pipe(spritesmith({
        imgName: 'icon.sprite.png',
        imgPath: '../img/icon.sprite.png',
        cssName: 'sprites.scss'
     }));

  spriteData.img.pipe(gulp.dest(root + 'img'));
  spriteData.css.pipe(gulp.dest(src + 'sass/sprite/'));
});

gulp.task('sass', function biuldCass() {

  gulp.src(src+'sass/main.scss')
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(gulp.dest(root + 'css'))
    .pipe(browserSync.stream({stream:true}));

});

// Compile HTML from Pug sources
gulp.task('templates', function buildHtml() {

  gulp.src(src + 'pug/templates/**/*.pug')
    .pipe(plugins.pug({
      client: false,
      pretty: true,
      debug:false,
      compileDebug:false,
      cache:true,
    }))
    .pipe(gulp.dest(root));

});
gulp.task('templates-watch',["templates"],syncReload);

// Watch for changes in files
gulp.task('watch', function watchChanges() {

  // Watch icons
  gulp.watch(src + 'icons/**/*', ['sprite']);

  // Watch images
  gulp.watch(src + 'images/**/*', ['images']);

  // Watch .scss files
  gulp.watch(src+'sass/**/*.scss', ['sass']);

  // Watch .pug files
  gulp.watch(src+'pug/**/**/*.pug', ['templates-watch']);

 });

/****************************************************\
*
*       ./ DEV
*
\****************************************************/

/****************************************************\
*
*       PRO
*
\****************************************************/
// clear dist cache
gulp.task('clear', function() {
   gulp.src([dist+"/*"],{read: false}).pipe(plugins.rimraf({ force: true }));
});

// Compile CSS from Sass files and minified output
gulp.task('cssmin', function buildCssMin() {

  gulp.src(root+'css/**/*.css')
    .pipe(plugins.cssmin())
    .pipe(gulp.dest(function(file) {
      var relative = file.base.split("/css/");
      destPath = dist+'css/'+relative[1];
      return destPath;
    }))
    ;
});

/**
 * concat all javascript file (not libs components)
 * when finish calling 'uglify' task
 */
gulp.task('uglify', function buildUglify() {
  gulp.src(root+"js/*.js")
    .pipe(plugins.uglify())
    .pipe(gulp.dest(function(file) {
      var relative = file.base.split("/js/");
      destPath = dist+'js/'+relative[1];
      return destPath;
    }))
    ;
});

gulp.task('exceDist',function buildExceDist(){
   gulp.src('./', {read: false})
   .pipe(plugins.exec('gulp clear'))
   .pipe(plugins.exec('gulp cssmin'))
   .pipe(plugins.exec('gulp uglify'));
});

gulp.task('execRelease',function buildExecRelease(){

  gulp.src('./', {read: false})
    .pipe(plugins.exec('gulp exceDist'))
    .pipe(plugins.exec('cp -r '+root+'img '+dist+'img'))
    .pipe(plugins.exec('cp -r '+root+'fonts '+dist+'fonts'))
    .pipe(plugins.exec('cp -r '+root+'js/vendor* '+dist+'js/'))
    .pipe(plugins.exec('cp -r '+root+'*.html '+dist))
    ;
});

/****************************************************\
*
*       ./ PRO
*
\****************************************************/

/****************************************************\
*
*       Task
*
\****************************************************/

// Default Task
gulp.task('default', ['templates', 'server', 'watch']);
// prepare dist
gulp.task('dist', ['execRelease']);
